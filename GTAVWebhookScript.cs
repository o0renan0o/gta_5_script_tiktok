using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GTA;
using GTA.Math;
using GTA.UI;
using GTAVWebhook;
using GTAVWebhook.Types;

public class GTAVWebhookScript : Script
{
    private HttpServer httpServer = new HttpServer();
    private bool isFirstTick = true;
    private List<Vehicle> spawnedVehicles = new List<Vehicle>();
    private List<Attacker> npcList = new List<Attacker>();
    private Vector3 lastPlayerPosition = Game.Player.Character.Position;
    
    private float invincibilityDuration = 120f; // 2 minutos em segundos
    
    private DateTime invincibilityEndTime;
    
    private bool isZeroGravityActive = false;
    private DateTime zeroGravityEndTime;
    
    private bool isBlackoutActive = false;
    private DateTime blackoutEndTime;
    
    private bool isMeteorShowerActive = false;
    private DateTime meteorShowerEndTime;

    private DateTime nextWeatherChangeTime;
    private bool isWeatherAnomalyActive = false;
    private Weather[] extremeWeathers = new Weather[]
    {
        Weather.Blizzard, Weather.Clearing, Weather.Foggy, Weather.Overcast,
        Weather.Raining, Weather.ThunderStorm, Weather.Snowing
    };
    
    Dictionary<Attacker, string> npcNames = new Dictionary<Attacker, string>();


    List<string> npcModelNames = new List<string>
    {
        "a_f_m_beach_01",
        "a_m_m_beach_01",
        "a_f_y_tourist_01",
        "a_m_y_skater_01",
        "s_m_y_cop_01",
        "s_f_y_cop_01",
        "a_m_m_fatlatin_01",
        "a_m_y_hipster_01",
        "a_f_y_hipster_04",
        "g_m_y_ballaeast_01",
        "g_f_y_ballas_01",
        "csb_grove_str_dlr",
        "a_m_m_business_01",
        "a_f_m_eastsa_01",
        "a_m_y_genstreet_02",
        // Adicione mais nomes de modelos conforme necessário
    };


    
    List<string> vehicleNames = new List<string>
    {
        "Felon",
        "Pony",
        "Journey",
        "Tractor2",
        "Sadler",
        "Ripley",
        "Proptrailer",
        "Visione",
        "Voltic2",
        "Turismor",
        "Tornado6",
        "Michelli",
        "Veto",
        "Coach",
        "Shamal",
        "Blimp",
        "Verus",
        "Bifta",
        "Pbus2",
        "Oppressor2",
        "Faggio2",
        "Rhino",
        "Apc",
        "Thruster",
        "Annihilator",
        "Predator",
        "Fixter",
        "Weevil",
        "Pounder",
        "Tropic",
        "Police",
        "Caddy",
        // Adicione outros nomes de veículos aqui
    };

    public GTAVWebhookScript()
    {
        Tick += OnTick;
        KeyUp += OnKeyUp;
        KeyDown += OnKeyDown;
    }

    private void OnTick(object sender, EventArgs e)
    {
        if (isFirstTick)
        {
            isFirstTick = false;

            Logger.Clear();

            try
            {
                httpServer.Start();
                Logger.Log("HttpServer listening on port " + httpServer.Port);
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to start HttpServer - " + ex.Message);
            }

        }

        while (npcList.Count > 100)
        {
            try
            {
                npcList[0].Remove();
                npcList.RemoveAt(0);
                Logger.Log("Attacker over limit removed");
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to remove old attacker - " + ex.Message);
            }

        }

        try
        {
            foreach (Attacker attacker in npcList)
            {
                attacker.DrawName();
            }
        }
        catch (Exception ex)
        {
            Logger.Log("Failed to draw attacker names - " + ex.Message);
        }

        CommandInfo command = httpServer.DequeueCommand();

        if (command != null)
        {
            try
            {
                ProcessCommand(command);
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to execute command " + command.cmd + ". Error: " + ex.Message);
            }
        }
        
        // Verifica se o tempo de invencibilidade expirou
        if (DateTime.Now >= invincibilityEndTime && Game.Player.Character.IsInvincible)
        {
            Game.Player.Character.IsInvincible = false; // Desativa a invencibilidade
            Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Vix... agora vc nao e mais imortal !!");
        }
        
        if (isZeroGravityActive)
        {
            // Verifica se o tempo de duração do efeito de gravidade zero já passou
            if (DateTime.Now >= zeroGravityEndTime)
            {
                isZeroGravityActive = false; // Desativa o efeito de gravidade zero
                Notification.Show("Gravidade zero desativada!");
            }
            else
            {
                // Aplica o efeito de gravidade zero a veículos e pedestres próximos
                Vector3 playerPosition = Game.Player.Character.Position;
                float radius = 50f; // Define o raio de ação em metros

                foreach (Vehicle vehicle in World.GetNearbyVehicles(playerPosition, radius))
                {
                    vehicle.ApplyForce(Vector3.WorldUp * 0.1f); // Aplica uma força leve para cima
                }

                foreach (Ped ped in World.GetNearbyPeds(playerPosition, radius))
                {
                    if (!ped.IsPlayer && !ped.IsInVehicle()) // Ignora o jogador e NPCs em veículos
                    {
                        ped.ApplyForce(Vector3.WorldUp * 0.05f); // Aplica uma força mais leve para cima nos pedestres
                    }
                }
            }
        }
        
        // Dentro do OnTick:
        if (isBlackoutActive && DateTime.Now >= blackoutEndTime)
        {
            World.Blackout = false; // Desativa o apagão
            isBlackoutActive = false;
            Notification.Show("Apagão total desativado!");
        }
        
        
        // Ativa a chuva de meteoros se estiver dentro do tempo definido
        if (isMeteorShowerActive && DateTime.Now <= meteorShowerEndTime)
        {
            // Chama a função que gera os meteoros aqui
            SpawnMeteor();
        }
        // Desativa a chuva de meteoros após o tempo de término
        else if (isMeteorShowerActive && DateTime.Now > meteorShowerEndTime)
        {
            isMeteorShowerActive = false;
            Notification.Show("Chuva de meteoros terminou!");
        }
        
        
        if (isWeatherAnomalyActive && DateTime.Now >= nextWeatherChangeTime)
        {
            // Seleciona um clima extremo aleatório do array
            var randomWeather = extremeWeathers[new Random().Next(extremeWeathers.Length)];
            World.TransitionToWeather(randomWeather, 5.0f);
        
            // Define o próximo momento para mudança de clima
            nextWeatherChangeTime = DateTime.Now.AddMinutes(1); // Muda o clima a cada minuto
        }

    }

    private void OnKeyDown(object sender, KeyEventArgs e)
    {
    }

    private void OnKeyUp(object sender, KeyEventArgs e)
    {

    }

    private void ProcessCommand(CommandInfo command)
    {
        switch (command.cmd)
        {
            case "kill":
                {
                    Game.Player.Character.Kill();
                    break;
                }
            case "start_meteor_shower":
                isMeteorShowerActive = true;
                meteorShowerEndTime = DateTime.Now.AddMinutes(1); // O evento dura 1 minuto
                Notification.Show("Chuva de meteoros iniciada!");
                break;

            case "toggle_weather_anomaly":
                isWeatherAnomalyActive = !isWeatherAnomalyActive;
                if (isWeatherAnomalyActive)
                {
                    nextWeatherChangeTime = DateTime.Now; // Inicia imediatamente
                    Notification.Show("Anomalia climática ativada!");
                }
                else
                {
                    World.TransitionToWeather(Weather.ExtraSunny, 5.0f); // Retorna a um clima padrão ao desativar
                    Notification.Show("Anomalia climática desativada.");
                }
                break;
            case "activate_blackout":
                if (!isBlackoutActive)
                {
                    isBlackoutActive = true;
                    World.Blackout = true; // Ativa o apagão
                    blackoutEndTime = DateTime.Now.AddMinutes(1); // Define o apagão para durar 1 minuto
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Apagão total ativado!");
                }
                break;
            case "spawn_vehicle_random":
            {
                // Seleciona aleatoriamente um nome de veículo
                string randomVehicleName = vehicleNames[new Random().Next(vehicleNames.Count)];
                
                try
                { 
                    
                    Vector3 playerPosition = Game.Player.Character.Position;
                    Vector3 spawnPosition = new Vector3(playerPosition.X, playerPosition.Y, playerPosition.Z + 1f);
                    
                    spawnedVehicles.Add(World.CreateVehicle(new Model(randomVehicleName), spawnPosition)); 
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Veículo criado: " + randomVehicleName);
                }
                catch (Exception ex)
                {
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Veículo erro: " + randomVehicleName);
                }
                
                break;
            }
            case "delete_current_vehicle":
            {
                if (Game.Player.Character.CurrentVehicle != null)
                {
                    Game.Player.Character.CurrentVehicle.Delete();
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Veículo atual excluído.");
                }
                else
                {
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Não é possível excluir o veículo atual porque o jogador não está em um veículo.");
                }
                break;
            }
            case "vehicle_rain":
            {
                // Define quantos veículos serão spawnados
                int numberOfVehicles = 30; // Ajuste conforme necessário

                for (int i = 0; i < numberOfVehicles; i++)
                {
                    // Escolhe um nome de veículo aleatório da lista
                    int randomIndex = new Random().Next(vehicleNames.Count);
                    string selectedVehicleName = vehicleNames[randomIndex];

                    // Converte o nome do veículo para um modelo
                    Model selectedVehicleModel = new Model(selectedVehicleName);

                    // Calcula uma posição aleatória no céu acima do jogador
                    Vector3 spawnPosition = Game.Player.Character.Position + new Vector3(new Random().Next(-20, 20), new Random().Next(-20, 20), 30);

                    // Certifica-se de que o modelo é válido e foi carregado
                    if (selectedVehicleModel.IsValid && selectedVehicleModel.Request(1000)) // Espera até 1000ms para o modelo ser carregado
                    {
                        // Cria o veículo na posição calculada
                        Vehicle newVehicle = World.CreateVehicle(selectedVehicleModel, spawnPosition);

                        // Aplica uma força inicial para os veículos caírem de forma mais natural e interessante
                        newVehicle.ApplyForce(new Vector3(new Random().Next(-50, 50), new Random().Next(-50, 50), -20));
                    }
                }

                Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Chuva de veículos maluco !!!");
                break;
            }
            case "remove_spawned_vehicles":
                {
                    try
                    {
                        while (spawnedVehicles.Count > 0)
                        {
                            Logger.Log("Removing vehicle: " + spawnedVehicles[0].DisplayName);
                            spawnedVehicles[0].Delete();
                            spawnedVehicles.RemoveAt(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log("Failed to remove spawned vehicles: " + ex.Message);
                    }

                    break;
                }
            case "repair_current_vehicle":
                {
                    if (Game.Player.Character.CurrentVehicle != null)
                    {
                        Game.Player.Character.CurrentVehicle.HealthFloat = Game.Player.Character.CurrentVehicle.MaxHealthFloat;
                        Logger.Log("CurrentVehicle Health restored to " + Game.Player.Character.CurrentVehicle.HealthFloat.ToString());
                    }
                    else
                    {
                        Logger.Log("Cannot repair current vehicle because player not in vehicle");
                    }
                    break;
                }
            case "explode_vehicle":
                {
                    Vehicle[] nearbyVehicles = World.GetNearbyVehicles(Game.Player.Character, 20);
                    foreach (Vehicle vehicle in nearbyVehicles)
                    {
                        vehicle.Explode();
                    }
                    break;
                }
            case "mini_tornado":
            {
                try
                {
                    Vector3 playerPosition = Game.Player.Character.Position;
                    float radius = 30.0f; // Raio de ação do tornado
                    float forceMagnitude = 100.0f; // Intensidade da força aplicada

                    // Obtém todos os veículos no raio de ação
                    foreach (Vehicle vehicle in World.GetNearbyVehicles(playerPosition, radius))
                    {
                        Vector3 directionFromPlayer = vehicle.Position - playerPosition;
                        directionFromPlayer.Normalize();
                        // Aplica uma força ascendente e radial para simular o efeito de ser pego pelo tornado
                        vehicle.ApplyForce(directionFromPlayer * forceMagnitude + new Vector3(0, 0, forceMagnitude));
                    }

                    // Faz o mesmo para os pedestres
                    foreach (Ped ped in World.GetNearbyPeds(Game.Player.Character, radius))
                    {
                        if (!ped.IsPlayer) // Ignora o jogador
                        {
                            Vector3 directionFromPlayer = ped.Position - playerPosition;
                            directionFromPlayer.Normalize();
                            ped.ApplyForce(directionFromPlayer * forceMagnitude + new Vector3(0, 0, forceMagnitude));
                        }
                    }

                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Mini-tornado ativado!");
                }
                catch (Exception ex)
                {
                    Logger.Log("Erro ao criar mini-tornado: " + ex.Message);
                }
    
                break;
            }
            case "save_player_position":
            {
                lastPlayerPosition = Game.Player.Character.Position;
                Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Posicao salva !");
                break;
            }
            case "teleporte_last_player_position":
            {
                Game.Player.Character.Position = lastPlayerPosition;
                Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Ups... vc foi teleportado !");
                break;
            }
            case "invencibility":
            {
                invincibilityEndTime = DateTime.Now.AddMinutes(2);
                Game.Player.Character.IsInvincible = true;
                Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Estamos Invenciveis !");
                break;
            }
            case "alien_invasion":
            {
                if (Game.Player.Character.IsInAir)
                {
                    Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Aliens nao podem ser invocados pois voce esta no ar !");
                    break;
                }

                int numAliens = 5; // Definindo quantos alienígenas quer spawnar

                for (int i = 0; i < numAliens; i++)
                {
                    Vector3 spawnPosition = Game.Player.Character.Position.Around(50); // Spawnar até 50 metros do jogador
                    Model alienModel = new Model(PedHash.MovAlien01); // Exemplo de modelo alienígena

                    if (alienModel.IsValid && alienModel.Request(1000)) // Solicita o carregamento do modelo
                    {
                        Ped alienPed = World.CreatePed(alienModel, spawnPosition);
            
                        if (alienPed != null)
                        {
                            // Aqui você equipa o alienígena com uma arma. Substitua 'WeaponHash.UnholyHellbringer' por qualquer arma que se assemelhe a uma arma laser no seu jogo.
                            // Se 'RayCarbine' ou 'RayPistol' não estiverem disponíveis, 'UnholyHellbringer' pode ser uma alternativa futurística.
                            WeaponHash weaponToGive = WeaponHash.UnholyHellbringer; // Exemplo de arma futurística
                            alienPed.Weapons.Give(weaponToGive, 1000, true, true);

                            // Adiciona o Ped à sua lista de NPCs, se necessário
                            // npcList.Add(alienPed); // Supondo que você mantenha uma lista de NPCs criados

                            Logger.Log("Alien spawned with laser weapon.");
                        }
                    }
                }
                Notification.Show("~b~IndicatorControl~w~ ~g~v1.0~w~ Invasão alienígena iniciada!");
                break;
            }
            case "zero_gravity_event":
                isZeroGravityActive = true;
                zeroGravityEndTime = DateTime.Now.AddMinutes(1); // Adiciona 1 minuto ao tempo atual
                Notification.Show("Gravidade zero ativada!");
                break;

            case "give_weapon":
                {
                    WeaponHash weaponHash;
                    if (Enum.TryParse<WeaponHash>(command.custom, out weaponHash))
                    {
                        Game.Player.Character.Weapons.Give(weaponHash, 9999, true, true);
                        Logger.Log("Weapon given: " + command.custom);
                    }
                    else
                    {
                        Logger.Log("Cannot parse weapon name: " + command.custom);
                    }
                    break;
                }
            case "set_max_weapon_ammo":
                {
                    if (!int.TryParse(command.custom, out int ammo))
                        ammo = 9999;

                    if (ammo > Game.Player.Character.Weapons.Current.MaxAmmo)
                        ammo = Game.Player.Character.Weapons.Current.MaxAmmo;

                    Game.Player.Character.Weapons.Current.Ammo = ammo;
                    break;
                }
            case "remove_all_weapons":
            {
                Game.Player.Character.Weapons.RemoveAll();
                break;
            }

            case "set_time":
                {
                    TimeSpan ts;
                    if (TimeSpan.TryParse(command.custom + ":00", out ts))
                    {
                        World.CurrentTimeOfDay = ts;
                        Logger.Log("Time set to: " + command.custom);
                    }
                    else
                    {
                        Logger.Log("Cannot parse TimeSpan: " + command.custom);
                    }

                    break;
                }
            case "set_weather":
                {
                    Weather weather;
                    if (Enum.TryParse<Weather>(command.custom, out weather))
                    {
                        World.Weather = weather;
                    }
                    else
                    {
                        Logger.Log("Cannot parse Weather: " + command.custom);
                    }
                    break;
                }
            case "increase_wanted":
                {
                    if (Game.Player.WantedLevel < 5)
                        Game.Player.WantedLevel += 1;
                    break;
                }
            case "decrease_wanted":
                {
                    if (Game.Player.WantedLevel > 0)
                        Game.Player.WantedLevel -= 1;
                    break;
                }
            case "max_wanted":
                {
                    Game.Player.WantedLevel = 5;
                    break;
                }
            case "add_money":
                {

                    if (!int.TryParse(command.custom, out int moneyToAdd))
                    {
                        Logger.Log("add_money needs a numeric value!");
                        break;
                    }

                    Game.Player.Money += moneyToAdd;

                    if (Game.Player.Money < 0)
                        Game.Player.Money = 0;

                    Logger.Log("Player Money set to " + Game.Player.Money.ToString());

                    break;
                }
            case "set_money":
                {

                    if (!int.TryParse(command.custom, out int moneyToSet))
                    {
                        Logger.Log("set_money needs a numeric value!");
                        break;
                    }

                    if (moneyToSet < 0)
                        moneyToSet = 0;

                    Game.Player.Money = moneyToSet;

                    Logger.Log("Player Money set to " + Game.Player.Money.ToString());

                    break;
                }
            case "spawn_attackers":
                {
                    if (Game.Player.Character.IsInAir)
                    {
                        Logger.Log("Cannot spawn attacker because Player IsInAir");
                        break;
                    }

                    if (!int.TryParse(command.custom, out int num))
                        num = 1;

                    if (num > 50) num = 50;

                    for (int i = 0; i < num; i++)
                    {
                        Logger.Log("Spawn Attacker");
                        Attacker npc = new Attacker(command.username, false);
                        npcList.Add(npc);
                    }

                    break;
                }
            case "spawn_attackers_and_shoot":
                {
                    if (Game.Player.Character.IsInAir)
                    {
                        Logger.Log("Cannot spawn attacker because Player IsInAir");
                        break;
                    }
                    
                    Random rnd = new Random();
                    // Seleciona um nome de modelo aleatoriamente da lista
                    string selectedModelName = npcModelNames[rnd.Next(npcModelNames.Count)];

                    // Cria o Attacker passando o nome do modelo selecionado
                    Attacker npc = new Attacker(selectedModelName, true);
                    npcList.Add(npc);
                    npcNames.Add(npc, command.username); // Exemplo: renan ...


                    Logger.Log($"Spawned Attacker with model: {selectedModelName}");

                    break;
                }
            case "spawn_attackers_and_shoot_numb":
            {
                if (Game.Player.Character.IsInAir)
                {
                    Logger.Log("Cannot spawn attacker because Player IsInAir");
                    break;
                }

                if (!int.TryParse(command.custom, out int num))
                    num = 1;

                if (num > 50) num = 50;

                for (int i = 0; i < num; i++)
                {
                    Logger.Log("Spawn Attacker with gun");
                    Attacker npc = new Attacker(command.username, true);
                    npcList.Add(npc);
                }

                break;
            }
            case "attackers_start_shooting":
                {
                    if (!int.TryParse(command.custom, out int duration))
                        duration = 30;

                    if (duration < 1) duration = 30;

                    foreach (Attacker attacker in npcList)
                    {
                        Logger.Log("Attacker start shooting");
                        attacker.StartShooting(duration);
                    }

                    break;
                }
            case "remove_attackers":
                {
                    if (!int.TryParse(command.custom, out int num))
                        num = 1;

                    for (int i = 0; i < num; i++)
                    {
                        if (npcList.Count > 0)
                        {
                            npcList[npcList.Count - 1].Remove();
                            npcList.RemoveAt(npcList.Count - 1);

                            Logger.Log("Attacker removed, remaining: " + npcList.Count.ToString());
                        }
                    }

                    break;
                }
            case "leave_car":
                {
                    if (Game.Player.Character.IsInVehicle())
                    {
                        Game.Player.Character.Task.LeaveVehicle(LeaveVehicleFlags.None);
                    }
                    break;
                }
            case "skydive":
                {
                    Game.Player.Character.Position = new Vector3(Game.Player.Character.Position.X, Game.Player.Character.Position.Y, Game.Player.Character.Position.Z + 400);
                    Game.Player.Character.Task.Skydive();
                    Logger.Log("Skydive started");
                    break;
                }
            case "increase_health":
                {
                    if (!int.TryParse(command.custom, out int healStep))
                        healStep = 20;

                    float newHealthScore = Game.Player.Character.HealthFloat + healStep;

                    if (newHealthScore < 0)
                        newHealthScore = 0;

                    if (newHealthScore > Game.Player.Character.MaxHealthFloat)
                        newHealthScore = Game.Player.Character.MaxHealthFloat;

                    Game.Player.Character.HealthFloat = newHealthScore;

                    Logger.Log("Health set to " + Game.Player.Character.HealthFloat.ToString());

                    break;
                }
            default:
                {
                    Logger.Log("Unknown Command " + command.cmd);
                    break;
                }
        }
    }
    private void SpawnMeteor()
    {
        Vector3 spawnLocation = Game.Player.Character.Position.Around(200) + new Vector3(0, 0, 500); // Spawn acima do jogador
        Vector3 targetLocation = Game.Player.Character.Position.Around(50); // Define o alvo próximo ao jogador

        // Criação do objeto meteoro (pode ser um modelo específico ou um substituto genérico)
        Model meteorModel = new Model("prop_rock_4_big2"); // Exemplo: usar uma rocha grande como meteoro
        if (meteorModel.IsValid)
        {
            meteorModel.Request(500); // Solicita o carregamento do modelo
            if (meteorModel.IsLoaded)
            {
                // Spawna o meteoro no ar e aplica força para simular a queda
                var meteor = World.CreateProp(meteorModel, spawnLocation, true, true);
                meteor.ApplyForce(Vector3.WorldDown * 100); // Ajuste a força conforme necessário

                // Você pode adicionar aqui um temporizador para destruir o meteoro após o impacto
                // e/ou gerar uma explosão no local de impacto.
            }
        }
    }
}
