BETA

# GTA5 TikTok Integration
GTA5 Plugin para controlar GTA5 via Webhook eventos do TikTok LIVE. Eu recomendo [TikFinity](https://tikfinity.zerody.one/) para configurar Webhooks em determinados eventos.

## Webhook
O padrao de comunicacao e http://127.0.0.1:6721/<command>:<param>`

Confira os webhooks aqui na pasta postman

Verifique se o caminho para seu jogo esta correto em GTAVWebhook no final:

<PropertyGroup>
<PostBuildEvent>COPY "$(TargetPath)" "C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V\scripts"</PostBuildEvent>
</PropertyGroup>